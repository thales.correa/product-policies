# Privacidade e Termos

## Instalar

```bash
gem install bundler
bundle install
```

## Executar

```bash
bundle exec jekyll serve --watch
```

## Links

- [/payment-link/privacy](http://127.0.0.1:4000/payment-link/privacy)
- [/payment-link/terms](http://127.0.0.1:4000/payment-link/terms)
